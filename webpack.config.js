// To remove and clean build folder
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const appConfig = require('./app.config.json')
const path = require("path")
module.exports = {
    entry: './src/index.js',
    plugins: [
        new webpack.ProvidePlugin({
            _: 'underscore',
            $: 'jquery'
        }),
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'Hidden Covid Party',
            template: 'src/index.ejs',
            templateParameters : {
                config: appConfig
            }
        })

    ],
    output: {
        filename : 'bundle.js', 
        path: path.resolve(__dirname,'dist')
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000
    },
    module: {
        rules: [
            {
                test: /\.ejs$/,
                exclude: [path.resolve( __dirname, 'src/index.ejs')],
                use: {
                    loader: 'ejs-loader',
                    options: {
                        variable: 'data'
                    }
                }
            },
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: ["@babel/plugin-proposal-class-properties", '@babel/plugin-proposal-object-rest-spread']
                    }
                }
            },
            {
                test: /\.s?css$/i,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader',
                ],
            },
            {   
                
                test: /\.(png|gif|jpg|jpeg|svg)$/i,
                use: ['file-loader']
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    'file-loader',
                ],
            },
        ]
    }
}