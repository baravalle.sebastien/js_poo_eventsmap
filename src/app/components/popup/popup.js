import app from '../../app'
import template from './popupComponent.ejs'

export class Popup{
    domPopup
    clickDeletePopup 

    constructor(event){
        const {title='', event_class='' ,location='',date='',end_date='',description='', info='', jours, heures }= event
        const JSON = {
            title,location,
            date: this.readableDate(date) ,
            end_date : this.readableDate(end_date),
            description, event_class,
            jours,heures
        }
        const html = template({ ressources:JSON})
        const docHtml = app.domParser.parseFromString(html, 'text/html')
        this.domPopup = docHtml.getElementById('popup_back')
        
        

    }
    readableDate(date){
        const dateSplit = date.split('-')
        const moisList = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout','Septembre','Octobre','Novembre','Décembre' ]
        const mois = moisList[(parseInt(dateSplit[1])-1 )]
        return dateSplit[2]+' '+mois+' '+dateSplit[0]
    }
    

    render( domContainer ){
        domContainer.append( this.domPopup)
        this.clickDeletePopup = $('#popup_back')
        this.clickDeletePopup.click( this.deletePopup.bind(this))


    }
    deletePopup(e){
        $('#popup_back').remove()
    }


}