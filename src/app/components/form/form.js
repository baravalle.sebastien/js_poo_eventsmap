import app from '../../app'
import template from './formComponent.ejs'
import { Event } from '../../models/evenement'

export class Form {
    domForm

    constructor(){
        //set DomFORM
        const html = template({})
        const docHtml = app.domParser.parseFromString(html, 'text/html')
        this.domForm = docHtml.getElementById('form_event')

        
        

    }

    render( domContainer ){
        domContainer.append( this.domForm)
        $('#form_event').submit(this.formSend.bind(this))
        $('#form_send_btn').click( this.formSend.bind(this))
        const dateNow = new Date()
        const dateToString = dateNow.getFullYear()+'-'+(dateNow.getMonth()+1)+'-'+dateNow.getDate() 
        $('#get_date').val(dateToString)
        $('#get_end_date').val(dateToString)

    }
    formSend(e){
        e.preventDefault()

        
        const location = $('#get_location').val()
        const title = $('#get_title').val()
        const date = $('#get_date').val()
        const end_date = $('#get_end_date').val()
        const description = $('#get_description').val()
        

        if( location && title && date && end_date && description ){

            this.getLocation( location, (location)=>{
                
                const eventModele ={
                    title,location,date,end_date,description
                }
                const newEvent = new Event( eventModele )
                newEvent.id = app.arrEvents.length
                app.arrEvents.push(newEvent)

                app.saveToStorage()
                app.map.makeMarkerAndListnerPopup(newEvent)

               $('#get_location').val(null)
               $('#get_title').val(null)
               $('#get_date').val(null)
               $('#get_end_date').val(null)
               $('#get_description').val(null)
        
            })

        } else {
            $('#form_ajax_error').text( "donnes manquantes ")
            

        }


    }
    getLocation(location, callback ){
        
        const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/'+encodeURI(location)+'.json?access_token=pk.eyJ1IjoiZGF2aWRnaGV0dG8iLCJhIjoiY2toMDljbWp6MGc3bjJxcHJ0MzhrZm43eiJ9.g34YNuXg1d5NnkutkjACHw&limit=1'

        fetch( url ).then( response => {
            if ( ! response.ok  ){
                $('#form_ajax_error').text( " une erreur est survenue "+ response.statusText)
                return
            }
            return response.json()
        }).then(data => {
            console.log(' RETOUR DATA JSON : ', data.features[0]);
            if ( data.features.length === 0) {
                $('#form_ajax_error').text( " une erreur est survenue "+ ' impossible de trouver cette adresse ')
                return


            } else {
                location = {
                    lon : data.features[0].center[0],
                    lat : data.features[0].center[1],
                    adresse: data.features[0].place_name
                }
            }

            callback(location)

        })
    }


}