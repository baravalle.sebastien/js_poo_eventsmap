import app from '../../app'

export class EventMarkerCtrl {
    onAdd(map){
        this._map = map
        this._container = document.createElement('div')
        this._container.classList.add('mapboxgl-ctrl')

        $(this._container).addClass('container_markers_icons')

        const allMarker = [ 'event_passed','event_soon','event_long_time']
        
        allMarker.forEach( markerType => {
            const newMarkerIcone = document.createElement('div')
            newMarkerIcone.className = "active marker_icone event_marker "+markerType
            
            $(newMarkerIcone).attr('id', markerType)
            const allElement = ['a', 'b', 'c']
            allElement.forEach(elementPart => {
    
                const newDiv =document.createElement('div')    
                // $(newDiv).attr('id', markerType)

                newDiv.innerHTML = elementPart
                newDiv.className = "marker_"+elementPart
                $(newMarkerIcone).append(newDiv)
            })
            $(this._container).append(newMarkerIcone)
            $(newMarkerIcone).click(this.hideMarker.bind(this))

        })

        return this._container
    }
    onRemove() {
        this._container.parentNode.removeChild(this._container);
        this._map = undefined;
    }

    hideMarker(e){
        const isActive = e.currentTarget.classList.contains('active')
        const currentTarget= e.currentTarget.id
        if( isActive ){
            e.currentTarget.classList.remove('active')
            console.log(currentTarget);
            $('div .'+currentTarget).hide()
            $('div.marker_icone.'+currentTarget  ).show().addClass('grise')

        } else {
            $('div .'+currentTarget).show()
            $('div.marker_icone.'+currentTarget  ).removeClass('grise')

            e.currentTarget.classList.add('active')
        }
    }
}