import mapboxgl from 'mapbox-gl'
import app from '../../app'
import{ Popup } from '../popup/popup'

const myMap = class mymap extends mapboxgl.Map {

    constructor(args){
        super(args)
    }
    makeMarkerAndListnerPopup(event){

        // MARKER PERSONALISE 
        const element = ((event)=>{
            let element = document.createElement('div')
            element.className= "event_marker "+event.event_class
            $(element).attr('id', event.id)
    
            const allElement = ['a', 'b', 'c']
            allElement.forEach(elementPart => {

                const newDiv =document.createElement('div')    
                $(newDiv).attr('id', event.id)
           
                newDiv.innerHTML = elementPart
                newDiv.className = "marker_"+elementPart
                $(element).append(newDiv)
            })
            return element

        })(event)

        let newEvent = new mapboxgl.Marker(element)
        newEvent
            .setLngLat( {
                lat:  event.location.lat,
                lng: event.location.lon
            }).addTo( this )
        $(element).click((e)=>{
            const id = e.target.id
            const event = app.arrEvents[id]
            app.popupComponent = new Popup(event) 
            app.popupComponent.render($('body'))

        })


    }



}

export {
    myMap,
    mapboxgl
}