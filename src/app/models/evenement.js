export class Event{
    title
    
    location
    date
    end_date
    description
    id
    event_class


    constructor( {title='',location='',date='',end_date='',description=''} ){

        this.title =  title
        this.location = location 
        this.date = date
        this.end_date = end_date
        this.description = description
        this.event_class= this.setClass()

    }
    setClass(){
        const currentDate = Date.now()
        const eventDate = Date.parse(this.date)
        const eventDateEnd = Date.parse(this.end_date)
        
        if ( currentDate > eventDate ){
            const value = currentDate > eventDateEnd ? 'event_passed' : 'event_now'

            return value
        } else if ( (currentDate + (60*60*24*1000*3)) > eventDate ){
            this.jours = this.heures =((eventDate- currentDate)/60/60/24/1000) 
            this.jours = Math.floor(this.heures)
            this.heures = Math.floor((this.heures- this.jours) *24  )
            return 'event_soon'
        }
            return 'event_long_time'
        

    }
    static fromJSON(json, i ){
        const newEvent = new Event(json)
        newEvent.id = i
        return newEvent
    }

}