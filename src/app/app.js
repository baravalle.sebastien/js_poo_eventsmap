import '../style/style.scss'
import{ Form } from './components/form/form'
import{ Event } from './models/evenement'
import { EventMarkerCtrl } from './components/map/EventMarkerCtrl'
import {myMap, mapboxgl } from './components/map/mymap'
class App {
    get storageName() {return 'event'}
    map
    domParser
  
    arrEvents = []
    
    popupComponent
    #_formComponent
    get formComponent() { return this.#_formComponent }

    domFormContainer

    btnEvenement

    constructor(){
        this.domParser = new DOMParser()
        this.btnEvenement = $('[class="evenement"]')
        this.domFormContainer = $('#form')
        mapboxgl.accessToken = "pk.eyJ1IjoiZGF2aWRnaGV0dG8iLCJhIjoiY2toMDljbWp6MGc3bjJxcHJ0MzhrZm43eiJ9.g34YNuXg1d5NnkutkjACHw"
    }
    start() {
        this.#_formComponent = new Form() 
        this.formComponent.render(this.domFormContainer)


        this.startMapBox()


    }


    startMapBox(){
        // : GET ALL EVENT INFO 
        const strData = localStorage.getItem(this.storageName)
        if ( strData ){
            const arrJSON = JSON.parse(strData)
            
            let i = 0
            for( let eventJSON of arrJSON ) {
                this.arrEvents.push( Event.fromJSON(eventJSON, i ))
                i++
            }

        }



        const centerMap = new mapboxgl.LngLat(5.2, 46.2)
        
        this.map = new myMap({
            container: 'map_frame',
            style :"mapbox://styles/mapbox/streets-v11",
            center: centerMap,
            zoom: 4.5
        })
        const navctrl = new mapboxgl.NavigationControl({
            showCompass :false
        })

        this.map.addControl(navctrl, 'bottom-right')


        for( let event of this.arrEvents ) {
            this.map.makeMarkerAndListnerPopup(event)
        }
        const currentEventMarkerCtrl = new EventMarkerCtrl()
        this.map.addControl( currentEventMarkerCtrl , 'top-left')


    }

    

    saveToStorage() {
        localStorage.setItem(this.storageName, JSON.stringify(this.arrEvents))
    }




}
const app = new App();

export default app